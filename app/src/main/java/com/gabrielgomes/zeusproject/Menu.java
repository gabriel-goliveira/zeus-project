package com.gabrielgomes.zeusproject;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class Menu extends AppCompatActivity {
    private ImageView control;
    private ImageView voice;
    private ImageView desenvolvimento;
    private ImageView infos;
    private AlertDialog.Builder alerta;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);


        control = (ImageView) findViewById(R.id.imgControl);
        voice = (ImageView) findViewById(R.id.imgVoice);
        desenvolvimento = (ImageView) findViewById(R.id.imgTec);
        infos = (ImageView) findViewById(R.id.imgInfo);

        control.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =  new Intent(Menu.this, Control.class);
                startActivity(i);
            }
        });

        voice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent x = new Intent(Menu.this, Zeus.class);
                startActivity(x);
            }
        });

        desenvolvimento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent xx = new Intent(Menu.this, Sobre.class);
                startActivity(xx);
            }
        });

        infos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent xi = new Intent(Menu.this, FAQ.class);
                startActivity(xi);
            }
        });


    }

    @Override
    public void onBackPressed() {


        alerta = new AlertDialog.Builder(Menu.this);
        alerta.setTitle("Deseja mesmo sair dessa página?");
        alerta.setMessage("Deseja efetuar o logout?");
        alerta.setPositiveButton("Sim",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        alerta.setNegativeButton("Não",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        alerta.create();
        alerta.show();

    }
}
