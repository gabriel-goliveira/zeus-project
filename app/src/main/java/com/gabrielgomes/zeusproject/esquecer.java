package com.gabrielgomes.zeusproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class esquecer extends AppCompatActivity {
    private TextView user;
    private TextView email;
    private String nome;
    private String endereco;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_esquecer);

        user = (TextView) findViewById(R.id.txtUser);
        email = (TextView) findViewById(R.id.txtEmail);

        Intent intent = getIntent();

        Bundle dados = intent.getExtras();

        nome=dados.getString("user");
        endereco=dados.getString("email");

        user.setText(nome);
        email.setText(endereco);
    }
}
