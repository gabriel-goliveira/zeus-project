package com.gabrielgomes.zeusproject;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class Sign extends AppCompatActivity {
    private TextView user;
    private TextView email;
    private TextView password;
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);

        user = (TextView) findViewById(R.id.txtUser);
        email = (TextView) findViewById(R.id.txtEmail);
        password = (TextView) findViewById(R.id.txtPassword);


    }


    public void cadastrar(View view){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (user.getText().toString().equals("") || email.getText().toString().equals("") || password.getText().toString().equals("")||user.getText().toString().equals("") && email.getText().toString().equals("") && password.getText().toString().equals("")) {
                    Toast.makeText(Sign.this, "Preencha todos os campos", Toast.LENGTH_LONG).show();
                } else {

                    Intent it = new Intent();
                    it.putExtra("user", user.getText().toString());
                    it.putExtra("email", email.getText().toString());
                    it.putExtra("senha", password.getText().toString());
                    setResult(Activity.RESULT_OK, it);
                    finish();
                }
            }
        },SPLASH_TIME_OUT);
    }
}
