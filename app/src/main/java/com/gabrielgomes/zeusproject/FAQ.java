package com.gabrielgomes.zeusproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class FAQ extends AppCompatActivity {
    private TextView faq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);

        faq = (TextView) findViewById(R.id.faq);

        faq.setText("1-Como fazer o carrinho funcionar via bluetooth?\n" +
                "Deve-se parear o bluetooth do celular com o aplicativo do Zeus com o bluetooth do carrinho. \n" +
                "\n" +
                "2-Quais versões do Android são suportadas pelo Zeus?\n" +
                "Qualquer versão pós Android 5.0.\n" +
                "\n" +
                "3-Como controlar o Zeus por comando de voz?\n" +
                "O comando de voz funciona basicamente como o mecanismo do comando de voz da Google.\n" +
                "\n" +
                "4-Como fazer pesquisas pelo aplicativo?\n" +
                "Basta utilizar o comando de voz, assim como o mecanismo de pesquisa de voz da Google.\n" +
                "\n" +
                "5-Quem devo contatar para suporte técnico?\n" +
                "Caso seja necessário o suporte técnico da CReative Solutions, deve-se enviar um email para zeus.project10@gmail.com.\n" +
                "\n" +
                "\n" +
                "Para mais informações acesse zeus-project-umesp.github.io\n" +
                "\n" +
                "\n");
    }
}
