package com.gabrielgomes.zeusproject;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import static android.widget.Toast.LENGTH_LONG;

public class Login extends AppCompatActivity implements TextWatcher, CompoundButton.OnCheckedChangeListener{

        private EditText user;
        private EditText senha;
        private Button login;
        private Button sing;
        private Button forgot;
        private SQLiteDatabase banco;
        private CheckBox check;
        ArrayList<String> emails;


        //SharedPreferences
        SharedPreferences sharedPreferences;
        SharedPreferences.Editor editor;
        private static final String PREFS_NAME = "prefs";
        private static final String KEY_REMEMBER = "remember";
        private static final String KEY_USER = "usuario";
        private static final String KEY_PASS = "senha";

        @Override
        protected void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //SharedPreferences
        sharedPreferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        //Banco
        banco = openOrCreateDatabase("login", MODE_PRIVATE, null);
        banco.execSQL("CREATE TABLE IF NOT EXISTS user(id INTEGER PRIMARY KEY AUTOINCREMENT,usuario VARCHAR, email VARCHAR, senha VARCHAR)");


        //caixas de texto
        user = (EditText) findViewById(R.id.txtUser);
        senha = (EditText) findViewById(R.id.txtPassword);

        //Buttons
        login = (Button) findViewById(R.id.btLogin);
        sing = (Button) findViewById(R.id.btSing);
        forgot = (Button) findViewById(R.id.btForgot);
        check = (CheckBox) findViewById(R.id.cb);




        sing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent is = new Intent(Login.this, Sign.class);
                startActivityForResult(is, 0);
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (user.getText().toString().equals("") ||  senha.getText().toString().equals("")||user.getText().toString().equals("") && senha.getText().toString().equals("")) {
                    Toast.makeText(Login.this, "Preencha todos os campos", LENGTH_LONG).show();
                }else {
                    verificar(user.getText().toString(), senha.getText().toString());
                    managePrefs();
                }
            }
        });

        if(sharedPreferences.getBoolean(KEY_REMEMBER, false)){
            check.setChecked(true);
        }else {
            check.setChecked(false);
            user.setText(sharedPreferences.getString(KEY_USER,""));
            senha.setText(sharedPreferences.getString(KEY_PASS,""));

            user.addTextChangedListener(this);
            senha.addTextChangedListener( this);
            check.setOnCheckedChangeListener(this);


        }

        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String textoUsuario = user.getText().toString();

                try {
                    Cursor cursor = banco.rawQuery("SELECT email FROM user WHERE usuario = '"+textoUsuario+"';", null);
                    int indiceEmail = cursor.getColumnIndex("email");

                    emails = new ArrayList<>();

                    cursor.moveToFirst();
                    while (cursor != null) {
                        emails.add(cursor.getString(indiceEmail));
                        cursor.moveToNext();
                    }
                    Intent ix = new Intent(Login.this, esquecer.class);
                    ix.putExtra("email", emails.get(0));
                    ix.putExtra("user", user.getText().toString());
                    startActivity(ix);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });



    }

        @Override
        protected void onActivityResult ( int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (Activity.RESULT_OK == resultCode) {
            String username = data.getStringExtra("user");
            String email = data.getStringExtra("email");
            String senha = data.getStringExtra("senha");
            //Toast.makeText(this, username + " " + email + " " + senha, Toast.LENGTH_LONG).show();
            //Inserir no banco
            //banco.execSQL("INSERT INTO user (usuario , email , senha) VALUES ('"+username+"','"+email+"','"+senha+"')");
            ContentValues cv = new ContentValues();
            cv.put("usuario", username);
            cv.put("senha", senha);
            cv.put("email", email);
            banco.insert("user", null, cv);
            Toast.makeText(this, "Cadastrado", LENGTH_LONG).show();
        }

    }


    public void verificar(String user, String senha){
        Log.d("verificar", "user " + user + " senha " + senha);
        String senhaBanco;
        try {
            Cursor cursor = banco.rawQuery("SELECT senha FROM user WHERE usuario = '"+user+"' and senha ='" + senha + "';", null);
            if(cursor.getCount() > 0){
                Intent ix = new Intent(this, Menu.class);
                startActivity(ix);
            }else {
                Toast.makeText(Login.this, "Senha ou usuário incorreto", Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    managePrefs();
    }

    @Override
    public void afterTextChanged(Editable s) {

    }



    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        managePrefs();
    }
    private void managePrefs(){
        if (check.isChecked()){
            editor.putString(KEY_USER,user.getText().toString().trim());
            editor.putString(KEY_PASS, senha.getText().toString().trim());
            editor.putBoolean(KEY_REMEMBER, true);
            editor.apply();
        }else {
            editor.putBoolean(KEY_REMEMBER,false);
            editor.remove(KEY_PASS);
            editor.remove(KEY_USER);
            editor.apply();
        }
    }
}
